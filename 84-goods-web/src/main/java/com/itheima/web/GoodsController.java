package com.itheima.web;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.GoodService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ZZH
 * data 2019.10.16 下午 10:08
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    //lb:负载均衡
    //timeout:超时
    //retries:
    //cluster:出现异常的解决策略
    @Reference(loadbalance = "roundrobin",timeout = 3000,cluster = "failfast")
    private GoodService goodService;

    @RequestMapping("/find")
    public String getGoodsNameById(Integer id) {

        return goodService.getGoodsNameById(id);
    }

}
