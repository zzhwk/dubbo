package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.service.GoodService;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author ZZH
 * data 2019.10.16 下午 8:45
 */
//是dubbo的，不是spring的
@Service(interfaceClass = GoodService.class)//com.alibaba.dubbo.config.annotation.Service
@Transactional//事务注解，看你的类是否有接口，有jdk，没有cglib
public class GoodServiceImpl implements GoodService {

    @Override
    public String getGoodsNameById(Integer id) {

        String result="tudou"+id;
        System.out.println(result+20881);
        return result;
    }
}
