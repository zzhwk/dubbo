package com.itheima.service.impl;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author ZZH
 * data 2019.10.17 上午 9:20
 */
public class Main {
    public static void main(String[] args) {
        new ClassPathXmlApplicationContext("applicationContext-service.xml");
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
