package com.itheima.service.impl3;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * @author ZZH
 * data 2019.10.17 上午 9:20
 */
public class Main3 {
    public static void main(String[] args) {
        new ClassPathXmlApplicationContext("applicationContext-service20883.xml");
        try {
            System.in.read();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
